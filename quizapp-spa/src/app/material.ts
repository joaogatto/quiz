import {
    MatButtonModule,
    MatListModule,
    MatIconModule,
    MatTooltipModule,
    MatDialogModule,
    MatInputModule,
    MatDividerModule,
    MatCheckboxModule,
    MatSnackBarModule
} from '@angular/material';
import { NgModule } from '@angular/core';

@NgModule({
  imports: [
      MatButtonModule,
      MatListModule,
      MatIconModule,
      MatTooltipModule,
      MatDialogModule,
      MatInputModule,
      MatDividerModule,
      MatCheckboxModule,
      MatSnackBarModule
      ],
  exports: [
      MatButtonModule,
      MatListModule,
      MatIconModule,
      MatTooltipModule,
      MatDialogModule,
      MatInputModule,
      MatDividerModule,
      MatCheckboxModule,
      MatSnackBarModule
      ],
})
export class MaterialModule { }
