import { PerguntaCadastro } from './PerguntaCadastro';

export interface PerguntaDialogData {
    editMode: boolean;
    pergunta: PerguntaCadastro;
}
