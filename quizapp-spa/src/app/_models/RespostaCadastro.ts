export interface RespostaCadastro {
    id?: number;
    descricao: string;
    correta: boolean;
}
