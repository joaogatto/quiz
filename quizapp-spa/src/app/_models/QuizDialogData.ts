export interface QuizDialogData {
    id: number;
    editMode: boolean;
}
