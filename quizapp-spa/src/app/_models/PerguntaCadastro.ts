import { RespostaCadastro } from './RespostaCadastro';

export interface PerguntaCadastro {
    id?: number;
    descricao: string;
    respostas: RespostaCadastro[];
}
