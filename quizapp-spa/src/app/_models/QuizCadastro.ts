import { PerguntaCadastro } from './PerguntaCadastro';

export interface QuizCadastro {
    id?: number;
    nome: string;
    dataInicio: Date;
    dataFim: Date;
    perguntas: PerguntaCadastro[];
}
