export interface QuizToList {
    id: number;
    nome: string;
    quantidadePerguntas: number;
}
