import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { PerguntaCadastro } from '../../_models/PerguntaCadastro';
import { RespostaCadastro } from '../../_models/RespostaCadastro';
import { PerguntaDialogData } from '../../_models/PerguntaDialogData';

@Component({
  selector: 'app-pergunta-dialog',
  templateUrl: './pergunta-dialog.component.html',
  styleUrls: ['./pergunta-dialog.component.css']
})
export class PerguntaDialogComponent implements OnInit {
  editMode = false;
  perguntaDialogRef: MatDialogRef<PerguntaDialogComponent>;
  pergunta: PerguntaCadastro = {
    descricao: '',
    respostas: []
  };

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: PerguntaDialogData,
    private dialog: MatDialog,
    private alerta: MatSnackBar) { }

    ngOnInit() {
      if (this.data.editMode === true) {
        this.pergunta = this.data.pergunta;
        this.editMode = this.data.editMode;
      }

    }

    fecharDialog() {
      this.perguntaDialogRef = this.dialog.getDialogById('perguntaDialog');
      this.perguntaDialogRef.close();
    }

    validaPergunta() {
      if (this.pergunta.descricao.length < 1) {
        return false;
      }

      if (this.pergunta.respostas.length < 1) {
        return false;
      }

      let corretas = 0;

      let respProblema = false;
      this.pergunta.respostas.forEach(resp => {
        if (resp.descricao.length < 1) {
          respProblema = true;
        }

        if (resp.correta === true) {
          corretas++;
        }
      });

      if (corretas === 0) {
        return false;
      }

      if (respProblema === false) {
        return true;
      } else {
        return false;
      }
    }

    adicionarResposta() {
      const novaResposta: RespostaCadastro = {
        descricao: '',
        correta: false
      };
      this.pergunta.respostas.push(novaResposta);
    }

    removerResposta(resposta) {
      const index = this.pergunta.respostas.findIndex(resp => resp.descricao === resposta);

      this.pergunta.respostas.splice(index, 1);
    }

    respostaChanged(resposta) {
      const index = this.pergunta.respostas.findIndex(resp => resp.descricao === resposta);

      if (this.pergunta.respostas[index].correta === true) {
        this.pergunta.respostas[index].correta = false;
        return 0;
      }

      this.pergunta.respostas.forEach(resp => {
        resp.correta = false;
      });


      this.pergunta.respostas[index].correta = true;
    }

    cadastrarPergunta() {
      if (this.validaPergunta() === false) {
        // tslint:disable-next-line:max-line-length
        this.alerta.open('O campo Descrição deve ser preenchido, todas as respostas devem ter sua descrição preenchida e uma delas deve ser a correta.', '', {
          panelClass: 'error-snackbar',
          duration: 5000
        });
      } else {
        this.perguntaDialogRef = this.dialog.getDialogById('perguntaDialog');

        this.perguntaDialogRef.close(this.pergunta);
      }
    }

  }
