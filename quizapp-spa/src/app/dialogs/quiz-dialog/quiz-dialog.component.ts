import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MatDialogConfig, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { QuizCadastro } from '../../_models/QuizCadastro';
import { PerguntaDialogComponent } from '../pergunta-dialog/pergunta-dialog.component';
import { QuizService } from '../../_services/quiz.service';
import { QuizDialogData } from '../../_models/QuizDialogData';
import { PerguntaDialogData } from '../../_models/PerguntaDialogData';
import { PerguntaCadastro } from '../../_models/PerguntaCadastro';

@Component({
  selector: 'app-quiz-dialog',
  templateUrl: './quiz-dialog.component.html',
  styleUrls: ['./quiz-dialog.component.css']
})
export class QuizDialogComponent implements OnInit {
  editMode = false;
  quizDialogRef: MatDialogRef<QuizDialogComponent>;
  perguntaDialogRef: MatDialogRef<PerguntaDialogComponent>;
  quiz: QuizCadastro = {
    nome: '',
    dataInicio: null,
    dataFim: null,
    perguntas: []
  };
  perguntaGuardada: PerguntaCadastro;


  constructor(
    @Inject(MAT_DIALOG_DATA) public data: QuizDialogData,
    private dialog: MatDialog,
    private quizService: QuizService,
    private alerta: MatSnackBar) { }

    ngOnInit() {
      if (this.data.editMode === true) {
        this.quizService.getQuizToEdit(this.data.id)
        .subscribe(res => {
          this.quiz = res;
          this.editMode = this.data.editMode;
        });
      }
    }

    fecharDialog() {
      this.quizDialogRef = this.dialog.getDialogById('quizDialog');
      this.quizDialogRef.close();
    }

    adicionarPergunta(pergId) {
      const index = this.quiz.perguntas.findIndex(p => p.id === pergId);
      const perguntaDialogData: PerguntaDialogData = {
        editMode: false,
        pergunta: undefined
      };

      if (pergId !== 0) {
        perguntaDialogData.pergunta = JSON.parse(JSON.stringify(this.quiz.perguntas[index]));
        perguntaDialogData.editMode = true;
      }

      const perguntaDialogConfig = new MatDialogConfig();
      perguntaDialogConfig.id = 'perguntaDialog';
      perguntaDialogConfig.data = perguntaDialogData;
      perguntaDialogConfig.width = '80%';
      perguntaDialogConfig.height = '550px';

      this.perguntaDialogRef = this.dialog.open(PerguntaDialogComponent, perguntaDialogConfig);

      this.perguntaDialogRef.afterClosed()
      .subscribe(perg => {
        if (perg) {
          if (perguntaDialogData.editMode === false) {
            this.quiz.perguntas.push(perg);
          } else {
            this.quiz.perguntas[index] = perg;
          }
        }
      });
    }

    removerPergunta(pergDesc) {
      const index = this.quiz.perguntas.findIndex(p => p.descricao === pergDesc);

      this.quiz.perguntas.splice(index, 1);
    }

    validaQuiz() {
      if (this.quiz.nome.length < 1) {
        return false;
      }

      if (this.quiz.perguntas.length < 1) {
        return false;
      }

      if (this.quiz.dataInicio === null || this.quiz.dataFim === null) {
        return false;
      }

      const inicio = new Date(this.quiz.dataInicio).getTime();
      const fim = new Date(this.quiz.dataFim).getTime();

      if (inicio > fim) {
        return false;
      } else {
        return true;
      }
    }

    cadastrarQuiz() {
      if (this.validaQuiz() === false) {
        // tslint:disable-next-line:max-line-length
        this.alerta.open('O campo Descrição deve estar preenchido, deve haver ao menos uma pergunta e Data Inicio não pode ser Maior que Data Fim', '', {
          panelClass: 'error-snackbar',
          duration: 5000
        });
      } else {
        this.quizDialogRef = this.dialog.getDialogById('quizDialog');

        if (this.editMode === false) {
          this.quizService.cadastrarQuiz(this.quiz).subscribe(res => {
            this.quizDialogRef.close(res);
          });
        } else {
          this.quizService.atualizarQuiz(this.quiz.id, this.quiz)
          .subscribe(res => {
            this.quizDialogRef.close(res);
            console.log(res);
          }, error => console.log(error));
        }
      }

    }
  }

