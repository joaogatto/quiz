import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, LOCALE_ID } from '@angular/core';
import { MaterialModule} from './material';

import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { MainComponent } from './main/main.component';
import { QuizDialogComponent } from './dialogs/quiz-dialog/quiz-dialog.component';
import { FormsModule } from '@angular/forms';
import { MatDatepickerModule, MatNativeDateModule } from '@angular/material';
import { PerguntaDialogComponent } from './dialogs/pergunta-dialog/pergunta-dialog.component';
import { HttpClientModule } from '@angular/common/http';
import { QuizService } from './_services/quiz.service';

@NgModule({
   declarations: [
      AppComponent,
      NavComponent,
      MainComponent,
      QuizDialogComponent,
      PerguntaDialogComponent
   ],
   imports: [
      BrowserModule,
      BrowserAnimationsModule,
      MaterialModule,
      FormsModule,
      MatDatepickerModule,
      MatNativeDateModule,
      HttpClientModule
   ],
   providers: [
       MatDatepickerModule,
       {
           provide: LOCALE_ID, useValue: 'pt'
       },
       QuizService
    ],
   bootstrap: [
      AppComponent
   ],
   entryComponents: [
    QuizDialogComponent,
    PerguntaDialogComponent
   ]
})
export class AppModule { }
