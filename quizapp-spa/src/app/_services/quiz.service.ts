import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { QuizToList } from '../_models/QuizToList';
import { QuizCadastro } from '../_models/QuizCadastro';

@Injectable({
  providedIn: 'root'
})
export class QuizService {

  apiUrl = 'http://localhost:5000/api/quiz/';

  constructor(private http: HttpClient) { }

  getQuizes(): Observable<QuizToList[]> {
    return this.http.get<QuizToList[]>(this.apiUrl + 'getAllQuizes');
  }

  getQuizToEdit(quizId: number): Observable<QuizCadastro> {
    return this.http.get<QuizCadastro>(this.apiUrl + 'getQuizToEdit/' + quizId);
  }

  cadastrarQuiz(quiz) {
    return this.http.post(this.apiUrl + 'cadastrarQuiz', quiz);
  }

  atualizarQuiz(quizId: number, quiz) {
    return this.http.put(this.apiUrl + 'atualizarQuiz/' + quizId, quiz);
  }

  removerQuiz(quizId) {
    return this.http.put(this.apiUrl + 'removerQuiz/' + quizId, null);
  }

}
