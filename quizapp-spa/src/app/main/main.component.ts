import { Component, OnInit } from '@angular/core';
import { QuizDialogComponent } from '../dialogs/quiz-dialog/quiz-dialog.component';
import { MatDialogRef, MatDialog, MatDialogConfig } from '@angular/material';
import { QuizCadastro } from '../_models/QuizCadastro';
import { QuizToList } from '../_models/QuizToList';
import { QuizService } from '../_services/quiz.service';
import { QuizDialogData } from '../_models/QuizDialogData';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  quizDialogRef: MatDialogRef<QuizDialogComponent>;
  quizes: QuizToList[] = [];
  quizDialogData: QuizDialogData = {
    id: 0,
    editMode: false
  };

  constructor(private dialog: MatDialog, private quizService: QuizService) { }

  ngOnInit() {
    this.quizService.getQuizes()
    .subscribe(res => this.quizes = res);
  }

  removerQuiz(quizId) {
    this.quizService.removerQuiz(quizId)
    .subscribe(res => {
      const index = this.quizes.findIndex(q => q.id === quizId);

      this.quizes.splice(index, 1);
    });
  }

  openQuizDialog(quizToOpen: number) {
    this.quizDialogData.id = quizToOpen;
    const index = this.quizes.findIndex(q => q.id === quizToOpen);

    if (quizToOpen === 0) {
      this.quizDialogData.editMode = false;
    } else {
      this.quizDialogData.editMode = true;
    }

    const quizDialogConfig = new MatDialogConfig();
    quizDialogConfig.id = 'quizDialog';
    quizDialogConfig.data = this.quizDialogData;
    quizDialogConfig.width = '80%';
    quizDialogConfig.height = '500px';

    this.quizDialogRef = this.dialog.open(QuizDialogComponent, quizDialogConfig);

    this.quizDialogRef.afterClosed()
    .subscribe(quiz => {
      if (quiz) {
        if (this.quizDialogData.editMode === false) {
          this.quizes.push(quiz);
        } else {
          this.quizes[index] = quiz;
        }
      }
    });
  }

}
