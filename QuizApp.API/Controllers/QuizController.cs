using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using QuizApp.API.Dtos;
using QuizApp.API.Models;
using QuizApp.API.Persistence;

namespace QuizApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QuizController : ControllerBase
    {
        private readonly IQuizRepository _repo;
        private readonly IMapper _mapper;
        public QuizController(IQuizRepository repo, IMapper mapper)
        {
            _repo = repo;
            _mapper = mapper;
        }

        [HttpPost("cadastrarQuiz")]
        public async Task<IActionResult> CadastrarQuiz(QuizCadastroDto quiz)
        {
            var quizModel = _mapper.Map<QuizCadastroDto, Quiz>(quiz);
            quizModel.Status = 1;

            var quizFromRepo = await _repo.CadastrarQuiz(quizModel);

            if(quizFromRepo == null) {
                return BadRequest();
            }

            var perguntasModel = _mapper.Map<IEnumerable<PerguntaCadastroDto>, IEnumerable<Pergunta>>(quiz.Perguntas);

            foreach (var pergunta in perguntasModel)
            {
                pergunta.QuizId = quizFromRepo.Id;
            }

            var perguntasFromRepo = await _repo.CadastrarPerguntas(perguntasModel);

            var quizRepo = await _repo.GetQuiz(quizFromRepo.Id);

            var quizReturn = _mapper.Map<Quiz, QuizToListDto>(quizRepo);

            return Ok(quizReturn);
        }

        [HttpGet("getAllQuizes")]
        public async Task<IActionResult> getAllQuizes()
        {
            var quizes = await _repo.GetAllQuizes();

            var quizesToList = _mapper.Map<IEnumerable<Quiz>, IEnumerable<QuizToListDto>>(quizes);

            return Ok(quizesToList);
        }

        [HttpGet("getQuizToEdit/{quizId}")]
        public async Task<IActionResult> getQuizToEdit(int quizId)
        {
            var quizRepo = await _repo.GetQuiz(quizId);

            var quizToEdit = _mapper.Map<Quiz, QuizToEditDto>(quizRepo);

            return Ok(quizToEdit);
        }

        [HttpPut("atualizarQuiz/{quizId}")]
        public async Task<IActionResult> atualizarQuiz(int quizId, QuizToEditDto quiz)
        {
            var quizFromRepo = await _repo.GetQuiz(quizId);
            if(quizFromRepo == null) {
                return NotFound();
            } else {
                _mapper.Map(quiz, quizFromRepo);
            }

            // addnovasperguntas
            var perguntasModel = _mapper.Map<IEnumerable<PerguntaCadastroDto>, IEnumerable<Pergunta>>(quiz.Perguntas);

            var novasPerguntas = perguntasModel.Where(p => p.Id == 0).ToList();

            foreach (var pergunta in novasPerguntas)
            {
                pergunta.QuizId = quiz.Id;
            }

            await _repo.CadastrarPerguntas(novasPerguntas);
            // addnovasperguntas

            //remove perguntas
            foreach (var pergunta in quizFromRepo.Perguntas.ToList())
            {
                var perguntaRetirada = true;

                foreach (var perguntaDto in quiz.Perguntas)
                {
                    if(pergunta.Id == perguntaDto.Id) 
                    {
                        perguntaRetirada = false;
                        
                        // add e remove respostas
                        foreach (var resposta in pergunta.Respostas.ToList())
                        {
                            var respostaRetirada = true;

                            foreach (var respostaDto in perguntaDto.Respostas)
                            {
                                if(respostaDto.Id == 0)
                                {
                                    var resp = _mapper.Map<RespostaCadastroDto, Resposta>(respostaDto);
                                    pergunta.Respostas.Add(resp);
                                }
                                else if (respostaDto.Id == resposta.Id)
                                {
                                    respostaRetirada = false;
                                    _mapper.Map(respostaDto, resposta);
                                }
                            }

                            if(respostaRetirada == true)
                            {
                                pergunta.Respostas.Remove(resposta);
                            }
                        }
                        // add e remove respostas
                    }
                }

                foreach (var novaPergunta in novasPerguntas)
                {
                    if(pergunta.Id == novaPergunta.Id) 
                    {
                        perguntaRetirada = false;
                    }
                }

                if(perguntaRetirada == true)
                {
                    quizFromRepo.Perguntas.Remove(pergunta);
                } 
            }
            //remove perguntas
            await _repo.SaveChanges();
            
            var quizReturn = _mapper.Map<Quiz, QuizToListDto>(quizFromRepo);

            return Ok(quizReturn);
        }

        [HttpPut("removerQuiz/{quizId}")]
        public async Task<IActionResult> removerQuiz(int quizId)
        {
            if (await _repo.RemoverQuiz(quizId))
                return NoContent();

            return BadRequest();
        }
    }
}