using System;
using System.Collections.Generic;

namespace QuizApp.API.Dtos
{
    public class QuizToEditDto
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public DateTime DataInicio { get; set; }
        public DateTime DataFim { get; set; }
        public IEnumerable<PerguntaCadastroDto> Perguntas { get; set; }
    }
}