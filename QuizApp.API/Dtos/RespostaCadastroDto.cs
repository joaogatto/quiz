namespace QuizApp.API.Dtos
{
    public class RespostaCadastroDto
    {
        public int Id { get; set; }
        public string Descricao { get; set; }
        public bool Correta { get; set; }
    }
}