using System;
using System.Collections.Generic;

namespace QuizApp.API.Dtos
{
    public class QuizCadastroDto
    {
        public string Nome { get; set; }
        public DateTime DataInicio { get; set; }
        public DateTime DataFim { get; set; }
        public ICollection<PerguntaCadastroDto> Perguntas { get; set; }
    }
}