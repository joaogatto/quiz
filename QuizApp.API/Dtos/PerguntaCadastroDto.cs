using System.Collections.Generic;

namespace QuizApp.API.Dtos
{
    public class PerguntaCadastroDto
    {
        public int Id { get; set; }
        public string Descricao { get; set; }
        public ICollection<RespostaCadastroDto> Respostas { get; set; }
    }
}