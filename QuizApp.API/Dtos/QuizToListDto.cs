namespace QuizApp.API.Dtos
{
    public class QuizToListDto
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public int QuantidadePerguntas { get; set; }
    }
}