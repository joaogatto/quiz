using System.Linq;
using AutoMapper;
using QuizApp.API.Dtos;
using QuizApp.API.Models;

namespace QuizApp.API.Helper
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            // CreateMap<x, y>();
            CreateMap<QuizCadastroDto, Quiz>()
                .ForMember(q => q.Perguntas, opt => opt.Ignore());

            CreateMap<PerguntaCadastroDto, Pergunta>();
            CreateMap<RespostaCadastroDto, Resposta>();
            CreateMap<QuizToEditDto, Quiz>()
                .ForMember(q => q.Status, opt => opt.Ignore())
                .ForMember(q => q.Perguntas, opt => opt.Ignore());


            CreateMap<Quiz, QuizToListDto>()
                .ForMember(ql => ql.QuantidadePerguntas, opt =>
                    opt.MapFrom(q => q.Perguntas.Count()));
            CreateMap<Quiz, QuizToEditDto>();
        }
    }
}