using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace QuizApp.API.Models
{
    public class Pergunta
    {
        public int Id { get; set; }
        public int QuizId { get; set; }
        public Quiz Quiz { get; set; }
        public string Descricao { get; set; }
        public ICollection<Resposta> Respostas { get; set; }

        public Pergunta()
        {
            this.Respostas = new Collection<Resposta>();
        }
    }
}