namespace QuizApp.API.Models
{
    public class Resposta
    {
        public int Id { get; set; }
        public int PerguntaId { get; set; }
        public Pergunta Pergunta { get; set; }
        public string Descricao { get; set; }
        public bool Correta { get; set; }
    }
}