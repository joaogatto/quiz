using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace QuizApp.API.Models
{
    public class Quiz
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public DateTime DataInicio { get; set; }
        public DateTime DataFim { get; set; }
        public int Status { get; set; }
        public ICollection<Pergunta> Perguntas { get; set; }

        public Quiz()
        {
            this.Perguntas = new Collection<Pergunta>();
        }
    }
}