using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using QuizApp.API.Dtos;
using QuizApp.API.Models;

namespace QuizApp.API.Persistence
{
    public class QuizRepository : IQuizRepository
    {
        private readonly QuizDbContext _context;
        private readonly IMapper _mapper;
        public QuizRepository(QuizDbContext context, IMapper mapper)
        {
            _mapper = mapper;
            _context = context;
        }

        public async Task<IEnumerable<Pergunta>> CadastrarPerguntas(IEnumerable<Pergunta> perguntas)
        {
            foreach (var pergunta in perguntas)
            {
                await _context.Perguntas.AddAsync(pergunta);
            }

            await _context.SaveChangesAsync();

            return perguntas;
        }

        public async Task<Quiz> CadastrarQuiz(Quiz quiz)
        {
            await _context.Quizes.AddAsync(quiz);

            await _context.SaveChangesAsync();

            return quiz;
        }

        public async Task<Quiz> GetQuiz(int id)
        {
            var quiz = await _context.Quizes
                .Include(q => q.Perguntas)
                    .ThenInclude(p => p.Respostas)
                .FirstOrDefaultAsync(q => q.Id == id);

            if (quiz == null)
                return null;

            return quiz;
        }

        public async Task<IEnumerable<Quiz>> GetAllQuizes()
        {
            var quizes = await _context.Quizes
                            .Include(q => q.Perguntas)
                            .Where(q => q.Status != 3)
                            .ToListAsync();

            return quizes;
        }

        public async Task<IEnumerable<Resposta>> GetRespostasDaPergunta(int perguntaId)
        {
            var respostas = await _context.Respostas
                                .Where(r => r.PerguntaId == perguntaId)
                                .ToListAsync();

            return respostas;
        }
        
        public async Task<bool> SaveChanges()
        {
            await _context.SaveChangesAsync();

            return true;
        }

        public async Task<bool> RemoverQuiz(int quizId)
        {
            var quiz = await _context.Quizes.FirstOrDefaultAsync(q => q.Id == quizId);

            if(quiz == null)
                return false;

            quiz.Status = 3;

            await _context.SaveChangesAsync();

            return true;
        }
    }
}