using System.Collections.Generic;
using System.Threading.Tasks;
using QuizApp.API.Dtos;
using QuizApp.API.Models;

namespace QuizApp.API.Persistence
{
    public interface IQuizRepository
    {
         Task<Quiz> CadastrarQuiz(Quiz quiz);
         Task<Quiz> GetQuiz(int id);
         Task<IEnumerable<Quiz>> GetAllQuizes();
         Task<IEnumerable<Pergunta>> CadastrarPerguntas(IEnumerable<Pergunta> perguntas);
         Task<IEnumerable<Resposta>> GetRespostasDaPergunta(int perguntaId);
         Task<bool> RemoverQuiz(int quizId);
         Task<bool> SaveChanges();
    }
}